package modules;

import configurations.TestBase;
import libraries.Guru99Functions;
import org.testng.annotations.Test;

public class Guru99Test extends TestBase {
    @Test(dataProvider = "guru99data")
    public static void guru_test(String fillText, String expectedText){
        Guru99Functions guPage = new Guru99Functions(driver);
        guPage.fillGuruForm(fillText);
        guPage.clickShowButton();
        guPage.verifyTextChange(expectedText);
    }
}
