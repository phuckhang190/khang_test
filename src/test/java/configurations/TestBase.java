package configurations;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.*;
import static supports.CommonFunctions.*;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class TestBase {
    public static AndroidDriver driver;

    @BeforeTest
    public static void setUp(){
        try{
            File app = new File("./app/android/Guru99App.apk");
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability(MobileCapabilityType.APP, app);
            caps.setCapability(MobileCapabilityType.PLATFORM_NAME, getDeviceInfo("platformName"));
            caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, getDeviceInfo("platformVersion"));
            caps.setCapability(MobileCapabilityType.DEVICE_NAME, getDeviceInfo("deviceName"));
            caps.setCapability("appPackage", getDeviceInfo("appPackage"));
            caps.setCapability("appActivity", getDeviceInfo("appActivity"));
//            caps.setCapability("noReset", true);
            driver = new AndroidDriver(new URL(getDeviceInfo("appiumHost")),caps);
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        } catch (MalformedURLException e){
            System.out.print(e);
        }

    }
    @DataProvider(name = "guru99data")
    public Object[][] testData() {
        return new Object[][] {
                new Object[] {"Guru99 Test","Guru99 Test"}
        };
    }

    @AfterMethod
    public void takeScreenShotIfFailure(ITestResult testResult) throws IOException {
        String screenShotFile;
        //Create format date
        String time = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        //Create the file name with date time format then grant to "screenShotFile"
        screenShotFile = System.getProperty("user.dir")+"/src/test/java/reports/images/"+ " Test_"+ time + ".png";
        if (testResult.getStatus() == ITestResult.FAILURE){
            File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File(screenShotFile));
        }
    }

    @AfterTest
    public void tearDown(){
        driver.quit();
    }
}
