Hello, this is a maven project which is made by Phuc Khang.

In order to run the script, make sure the environments below are available:
- Device is connected to PC
- Install JDK, Android sdk
- Install Maven (https://www.mkyong.com/maven/how-to-install-maven-in-windows/)
- Install Appium:
	+ Install nodeJS https://nodejs.org/en/
	+ Use command lines below to install:
	    + npm install appium -g
	    + npm install wd
		
Run project:
- Start appium server by command line: appium (Port default is 4723)
- Redirect to project folder (in this case, it is khang_test)
    + Use following command line to run: mvn install
	
Components in project:
- Config the android device in following path: ./khang_test/src/test/java/configurations/Android.properties
- Config data in @DataProvider(name = "guru99data") by following path: ./khang_test/src/test/java/configurations/TestBase.java 
	+ In this project, data is small, so we can define in method testData() directly. (No need cvs, json, etc.)
- The test report is in following folder: ./khang_test/src/test/java/reports
	+ Open index.html to see the report
	+ When the test is fail, there is a screen which is in folder reports/images
